\c explorausp_mpc


/*
 * CREATE
 */

CREATE OR REPLACE FUNCTION insere_ministra (id_prof INT, id_disc INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.Ministra (ID_professor, ID_disciplina) VALUES (id_prof, id_disc)
      RETURNING ID_ministra INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_oferecimento (id_ministrante INT, id_sigla INT, id_resp INT,
  periodo VARCHAR, turma INT, vagas INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    SELECT insere_ministra ($1, $2) INTO id;

    INSERT INTO Integracao_PC.Oferecimento (ID_ministra, ID_professor_responsavel, Periodo, Turma,
      Vagas) VALUES (id, id_resp, $4, $5, $6)
      RETURNING ID_oferecimento INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_cursa (id_aluno INT, id_oferec INT, optativa VARCHAR,
  nota_aluno FLOAT, frequencia_aluno INT, status_aluno VARCHAR)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.cursa (ID_aluno, ID_oferecimento, Optativa, Nota_aluno,
      Frequencia_aluno, Status_aluno) VALUES (id_aluno, id_oferec, $3, $4, $5, $6)
      RETURNING ID_cursa INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_al_curr (id_al INT, id_curr INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.al_curr (ID_aluno, ID_curriculo)
      VALUES (id_al, id_curr)
      RETURNING ID_al_curr INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_planeja_disciplina (id_aluno INT, id_disc INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.Planeja_disciplina (ID_aluno, ID_disciplina) VALUES (id_aluno, id_disc)
      RETURNING ID_planeja_disciplina INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_planeja_modulo (id_aluno INT, id_mod INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.Planeja_modulo (ID_aluno, ID_modulo) VALUES (id_aluno, id_mod)
      RETURNING ID_planeja_modulo INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_planeja_trilha (id_aluno INT, id_trilh INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.Planeja_trilha (ID_aluno, ID_trilha) VALUES (id_aluno, id_trilh)
      RETURNING ID_planeja_trilha INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_administra (id_adm INT, id_curr INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_PC.Administra (ID_administrador, ID_curriculo) VALUES (id_adm, id_curr)
      RETURNING ID_administra INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * UPDATE
 */

CREATE OR REPLACE FUNCTION atualiza_oferecimento (
  id_disciplina    INT,
  turma            INT,
  n_id_responsavel INT     DEFAULT NULL,
  n_periodo        VARCHAR DEFAULT NULL,
  n_turma          INT     DEFAULT NULL,
  n_vagas          INT     DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    id INT;
    old RECORD;
  BEGIN
    SELECT * INTO old FROM Integracao_PC.Oferecimento AS o WHERE ID_ministra IN
      (SELECT ID_ministra FROM Integracao_PC.Ministra AS m WHERE m.ID_disciplina = $1)
      AND o.Turma = $2;
    
    WITH updated AS (
      UPDATE Integracao_PC.Oferecimento AS o
        SET
          ID_professor_responsavel = COALESCE($3, old.ID_professor_responsavel),
          Periodo                  = COALESCE($4, old.Periodo),
          Turma                    = COALESCE($5, old.Turma),
          Vagas                    = COALESCE($6, old.Vagas)
        WHERE o.ID_oferecimento = old.ID_oferecimento RETURNING o.ID_oferecimento
    ) SELECT INTO id ID_oferecimento FROM updated;
    
    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_cursa (
  id_aluno      INT,
  id_disciplina INT,
  turma         INT,
  n_optativa    VARCHAR DEFAULT NULL,
  n_nota        FLOAT   DEFAULT NULL,
  n_frequencia  INT     DEFAULT NULL,
  n_status      VARCHAR DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    id_ofer INT;
    old RECORD;
    id INT;
  BEGIN
    SELECT ID_oferecimento INTO id_ofer FROM Integracao_PC.Oferecimento AS o WHERE ID_ministra IN
      (SELECT ID_ministra FROM Integracao_PC.Ministra AS m WHERE m.ID_disciplina = $2)
      AND o.Turma = $3;
    
    SELECT * INTO old FROM Integracao_PC.Cursa AS c WHERE c.ID_aluno = $1 AND
      c.ID_oferecimento = id_ofer;

    WITH updated AS (
      UPDATE Integracao_PC.Cursa AS c
      SET
        Optativa         = COALESCE($4, old.Optativa),
        Nota_aluno       = COALESCE($5, old.Nota_aluno),
        Frequencia_aluno = COALESCE($6, old.Frequencia_aluno),
        Status_aluno     = COALESCE($7, old.Status_aluno)
      WHERE c.ID_cursa = old.ID_cursa RETURNING old.ID_cursa
    ) SELECT INTO id ID_cursa FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * DELETE
 */

CREATE OR REPLACE FUNCTION remove_ministra (id_prof INT, id_disc INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_PC.Ministra WHERE ID_professor = $1 AND ID_disciplina = $2
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_oferecimento (id_disc INT, turma INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_minis INT;
    n INT;
  BEGIN
    SELECT ID_ministra INTO id_minis FROM Integracao_PC.Ministra WHERE ID_disciplina = $1;
    
    SELECT o.ID_ministra INTO id_minis FROM Integracao_PC.Oferecimento AS o WHERE ID_ministra IN
      (SELECT ID_ministra FROM Integracao_PC.Ministra WHERE ID_disciplina = $1)
      AND o.Turma = $2;

    WITH deleted AS (
      DELETE FROM Integracao_PC.Oferecimento AS o WHERE o.ID_ministra = id_minis AND o.Turma = $2
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_cursa (id_alun INT, id_disc INT, turma INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_ofer INT;
    n INT;
  BEGIN
    SELECT ID_oferecimento INTO id_ofer FROM Integracao_PC.Oferecimento AS o WHERE ID_ministra IN
      (SELECT ID_ministra FROM Integracao_PC.Ministra WHERE ID_disciplina = $2)
      AND o.Turma = $3;

    WITH deleted AS (
      DELETE FROM Integracao_PC.Cursa WHERE ID_aluno = $1 AND ID_oferecimento = id_ofer
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_al_curr (id_alun INT, id_curr INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_PC.al_curr WHERE ID_aluno = id_alun AND ID_curriculo = id_curr
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_planeja_disciplina (id_alun INT, id_disc INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_PC.Planeja_disciplina WHERE ID_aluno = id_alun AND
      ID_disciplina = id_disc RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_planeja_modulo (id_alu INT, id_mod INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_PC.Planeja_modulo WHERE ID_aluno = id_alu AND ID_modulo = id_mod
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_planeja_trilha (id_alu INT, id_tri INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_PC.Planeja_trilha WHERE ID_aluno = id_alu AND ID_trilha = id_tri
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_administra (id_adm INT, id_curr INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_PC.Administra WHERE ID_administrador = id_adm AND
      ID_curriculo = id_curr RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * RETRIEVAL
 */

CREATE OR REPLACE FUNCTION disciplinas_pretendidas (id_aluno VARCHAR) RETURNS TABLE (
  ID_disciplina VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      pd.ID_disciplina
    FROM Integracao_PC.Planeja_disciplina AS pd WHERE pd.ID_aluno = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION modulos_pretendidos (id_aluno VARCHAR) RETURNS TABLE (
  ID_modulo VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      pm.ID_modulo
    FROM Integracao_PC.Planeja_modulo AS pm WHERE pm.ID_aluno = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION trilhas_pretendidas (id_aluno VARCHAR) RETURNS TABLE (
  ID_trilha VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      pt.ID_trilha
    FROM Integracao_PC.Planeja_trilha AS pt WHERE pt.ID_aluno = $1;
  END;
$$
LANGUAGE plpgsql;
