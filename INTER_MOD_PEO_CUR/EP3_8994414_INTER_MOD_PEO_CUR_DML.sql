\c explorausp_mpc

INSERT INTO Integracao_PC.Ministra
  (ID_professor, ID_disciplina)
  VALUES
  (1, 1),
  (2, 2),
  (2, 2),
  (4, 2),
  (5, 5),
  (4, 5),
  (5, 5),
  (3, 6),
  (4, 9),
  (1, 10);

INSERT INTO Integracao_PC.Oferecimento
  (ID_ministra, ID_professor_responsavel, Periodo, Turma, Vagas)
  VALUES
  (2,  1, 'quadrimestre-1-2016', 32432, 50),
  (1,  1, 'quadrimestre-2-2017', 32236, 60),
  (10, 2, 'quadrimestre-3-2014', 12432, 50),
  (8,  2, 'quadrimestre-4-2019', 32432, 65),
  (4,  3, 'quadrimestre-1-2019', 21325, 30),
  (7,  3, 'semestre-1-2019',     43212, 90),
  (9,  4, 'semestre-2-2013',     32532, 60),
  (4,  4, 'semestre-1-2016',     43643, 45),
  (3,  5, 'semestre-2-2014',     21468, 75),
  (6,  5, 'semestre-1-2015',     42865, 100);

INSERT INTO Integracao_PC.cursa
  (ID_aluno, ID_oferecimento, Optativa, Nota_aluno, Frequencia_aluno, Status_aluno)
  VALUES
  (1,  1, 'Eletiva',     5.5,  70,   'A'),
  (1,  2, 'Obrigatória', 8,    50,   'RF'),
  (2,  3, 'Livre',       3.8,  85,   'RN'),
  (2,  4, 'Eletiva',     NULL, NULL, 'MA'),
  (3,  5, 'Obrigatória', NULL, NULL, 'MA'),
  (3,  6, 'Eletiva',     NULL, NULL, 'I'),
  (4,  7, 'Eletiva',     4.0,  100,  'RA'),
  (4,  8, 'Eletiva',     NULL, NULL, 'T'),
  (4,  9, 'Obrigatória', 8.3,  100,  'A'),
  (5, 10, 'Obrigatória', 9.0,  90,   'A'),
  (5, 10, 'Livre',       9.0, 100,   'A'),
  (5,  7, 'Eletiva',     7.2,  90,   'A'),
  (5,  2, 'Obrigatória', 4.1,  80,   'RN');

INSERT INTO Integracao_PC.al_curr
  (ID_aluno, ID_curriculo)
  VALUES
  (1, 1),
  (2, 2),
  (3, 4),
  (4, 3),
  (5, 1);

INSERT INTO Integracao_PC.Planeja_trilha
  (ID_aluno, ID_trilha)
  VALUES
  (1, 1),
  (1, 2),
  (2, 4),
  (2, 6),
  (3, 1),
  (3, 8),
  (4, 9),
  (5, 9),
  (5, 1),
  (5, 4);

INSERT INTO Integracao_PC.Planeja_modulo
  (ID_aluno, ID_modulo)
  VALUES
  (1, 1),
  (1, 2),
  (2, 3),
  (2, 4),
  (3, 1),
  (3, 7),
  (4, 8),
  (5, 8),
  (5, 1),
  (5, 3);

INSERT INTO Integracao_PC.Planeja_disciplina
  (ID_aluno, ID_disciplina)
  VALUES
  (1, 2),
  (1, 5),
  (1, 5),
  (2, 5),
  (2, 3),
  (2, 1),
  (3, 3),
  (3, 2),
  (4, 10),
  (5, 9),
  (5, 5),
  (5, 6);

INSERT INTO Integracao_PC.Administra
  (ID_administrador, ID_curriculo)
  VALUES
  (1, 10),
  (1, 9),
  (2, 8),
  (2, 7),
  (3, 6),
  (3, 5),
  (3, 4),
  (4, 3),
  (4, 2),
  (4, 1);
