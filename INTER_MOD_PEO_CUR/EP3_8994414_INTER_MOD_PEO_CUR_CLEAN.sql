\c explorausp_mpc

DELETE FROM Integracao_PC.Ministra; 
ALTER SEQUENCE Integracao_PC.Ministra_id_ministra_seq RESTART WITH 1;

DELETE FROM Integracao_PC.Oferecimento; 
ALTER SEQUENCE Integracao_PC.Oferecimento_id_oferecimento_seq RESTART WITH 1;

DELETE FROM Integracao_PC.cursa; 
ALTER SEQUENCE Integracao_PC.Cursa_id_cursa_seq RESTART WITH 1;

DELETE FROM Integracao_PC.al_curr; 
ALTER SEQUENCE Integracao_PC.al_curr_id_al_curr_seq RESTART WITH 1;

DELETE FROM Integracao_PC.Planeja_trilha; 
ALTER SEQUENCE Integracao_PC.Planeja_trilha_id_planeja_trilha_seq RESTART WITH 1;

DELETE FROM Integracao_PC.Planeja_modulo; 
ALTER SEQUENCE Integracao_PC.Planeja_modulo_id_planeja_modulo_seq RESTART WITH 1;

DELETE FROM Integracao_PC.Planeja_disciplina; 
ALTER SEQUENCE Integracao_PC.Planeja_disciplina_id_planeja_disciplina_seq RESTART WITH 1;

DELETE FROM Integracao_PC.Administra; 
ALTER SEQUENCE Integracao_PC.Administra_id_administra_seq RESTART WITH 1;
