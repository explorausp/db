CREATE DATABASE ExploraUsp_MPC;
\c explorausp_mpc
CREATE SCHEMA IF NOT EXISTS Integracao_PC;


DROP TABLE IF EXISTS Integracao_PC.Ministra CASCADE;
CREATE TABLE Integracao_PC.Ministra
(
  ID_ministra   SERIAL,
  ID_professor  INT NOT NULL,
  ID_disciplina INT NOT NULL,
  CONSTRAINT PK_ministra PRIMARY KEY (ID_ministra),
  CONSTRAINT CK_ministra_prof_disc UNIQUE (ID_professor, ID_disciplina)
);

DROP TABLE IF EXISTS Integracao_PC.Oferecimento CASCADE;
CREATE TABLE Integracao_PC.Oferecimento
(
  ID_oferecimento          SERIAL,
  ID_ministra              INT,
  ID_professor_responsavel INT,
  Periodo                  VARCHAR(19) NOT NULL,
  Turma                    INT         NOT NULL,
  Vagas                    INT         NOT NULL,
  CONSTRAINT PK_oferecimento PRIMARY KEY (ID_oferecimento),
  CONSTRAINT CK_oferecimento_minis_turma UNIQUE (ID_ministra, Turma),
  CONSTRAINT checa_turma CHECK (Turma >= 10000 AND Turma <= 9999999),
  CONSTRAINT checa_vagas CHECK (Vagas > 0 AND Vagas < 1000)
);

DROP TABLE IF EXISTS Integracao_PC.cursa CASCADE;
CREATE TABLE Integracao_PC.cursa
(
  ID_cursa         SERIAL,
  ID_aluno         INT         NOT NULL,
  ID_oferecimento  INT         NOT NULL,
  Optativa         VARCHAR(11) NOT NULL,
  Nota_aluno       FLOAT(1),
  Frequencia_aluno INT,
  Status_aluno     VARCHAR(2)  NOT NULL,
  CONSTRAINT PK_cursa PRIMARY KEY (ID_cursa),
  CONSTRAINT CK_cursa_aluno_oferec UNIQUE (ID_aluno, ID_oferecimento),
  CONSTRAINT checa_optativa   CHECK (Optativa IN ('Obrigatória', 'Eletiva', 'Livre')),
  CONSTRAINT checa_nota       CHECK (Nota_aluno >= 0 AND Nota_aluno <= 10),
  CONSTRAINT checa_frequencia CHECK (Frequencia_aluno >= 0 AND Frequencia_aluno <= 100),
  CONSTRAINT checa_status     CHECK (Status_aluno IN ('A', 'DI', 'I', 'MA', 'RA', 'RF', 'RN', 'T'))
);

DROP TABLE IF EXISTS Integracao_PC.al_curr CASCADE;
CREATE TABLE Integracao_PC.al_curr
(
  ID_al_curr   SERIAL,
  ID_aluno     INT    NOT NULL,
  ID_curriculo INT    NOT NULL,
  CONSTRAINT PK_al_curr PRIMARY KEY (ID_al_curr),
  CONSTRAINT CK_al_curr_aluno_curric UNIQUE (ID_aluno, ID_curriculo)
);

DROP TABLE IF EXISTS Integracao_PC.Planeja_trilha CASCADE;
CREATE TABLE Integracao_PC.Planeja_trilha
(
  ID_planeja_trilha SERIAL,
  ID_aluno          INT NOT NULL,
  ID_trilha         INT NOT NULL,
  CONSTRAINT PK_planeja_trilha PRIMARY KEY (ID_planeja_trilha),
  CONSTRAINT CK_planeja_trilha_aluno_trilha UNIQUE (ID_aluno, ID_trilha)
);

DROP TABLE IF EXISTS Integracao_PC.Planeja_modulo CASCADE;
CREATE TABLE Integracao_PC.Planeja_modulo
(
  ID_planeja_modulo SERIAL,
  ID_aluno          INT NOT NULL,
  ID_modulo         INT NOT NULL,
  CONSTRAINT PK_planeja_modulo PRIMARY KEY (ID_planeja_modulo),
  CONSTRAINT CK_planeja_modulo_aluno_modulo UNIQUE (ID_aluno, ID_modulo)
);

DROP TABLE IF EXISTS Integracao_PC.Planeja_disciplina CASCADE;
CREATE TABLE Integracao_PC.Planeja_disciplina
(
  ID_planeja_disciplina SERIAL,
  ID_aluno              INT NOT NULL,
  ID_disciplina         INT NOT NULL,
  CONSTRAINT PK_planeja_disciplina PRIMARY KEY (ID_planeja_disciplina),
  CONSTRAINT CK_planeja_disciplina UNIQUE (ID_aluno, ID_disciplina)
);

DROP TABLE IF EXISTS Integracao_PC.Administra CASCADE;
CREATE TABLE Integracao_PC.Administra
(
  ID_administra    SERIAL,
  ID_administrador INT NOT NULL,
  ID_curriculo     INT NOT NULL,
  CONSTRAINT PK_administra PRIMARY KEY (ID_administra),
  CONSTRAINT CK_administra_adm_curr UNIQUE (ID_administrador, ID_curriculo)
);
