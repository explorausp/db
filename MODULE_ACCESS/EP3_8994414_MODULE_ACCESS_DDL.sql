CREATE DATABASE ExploraUsp_MA;
\c explorausp_ma
CREATE SCHEMA IF NOT EXISTS Acesso;


-- Checagem de email
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS citext;
DROP DOMAIN IF EXISTS email CASCADE;
CREATE DOMAIN email AS citext
  CHECK ( value ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$' );


DROP TABLE IF EXISTS Acesso.Usuario CASCADE;
CREATE TABLE Acesso.Usuario
(
	ID_usuario  SERIAL,
	Email       email,
	Senha       TEXT NOT NULL,
	CONSTRAINT PK_usuario PRIMARY KEY (ID_usuario),
	CONSTRAINT SK_usuario UNIQUE (Email)
);

DROP TABLE IF EXISTS Acesso.Perfil CASCADE;
CREATE TABLE Acesso.Perfil 
(
  ID_perfil SERIAL,
  Nome      VARCHAR(20),
  Descricao VARCHAR(1000),
  CONSTRAINT PK_perfil      PRIMARY KEY (ID_perfil),
  CONSTRAINT SK_perfil_nome UNIQUE (Nome)
);

DROP TABLE IF EXISTS Acesso.Servico CASCADE;
CREATE TABLE Acesso.Servico
(
  ID_servico SERIAL,
  Nome       VARCHAR(50),
  Descricao  VARCHAR(1000),
  CONSTRAINT PK_servico      PRIMARY KEY (ID_servico),
  CONSTRAINT SK_servico_nome UNIQUE (Nome)
);

DROP TABLE IF EXISTS Acesso.us_pf CASCADE;
CREATE TABLE Acesso.us_pf
(
  ID_us_pf   SERIAL,
  ID_usuario INT NOT NULL,
  ID_perfil  INT NOT NULL,
  CONSTRAINT PK_us_pf PRIMARY KEY (ID_us_pf),
  CONSTRAINT FK_us_pf_usuario
    FOREIGN KEY (ID_usuario) REFERENCES Acesso.Usuario(ID_usuario)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT FK_us_pf_perfil 
    FOREIGN KEY (ID_perfil) REFERENCES Acesso.Perfil(ID_perfil)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_us_pf_usuario_perfil UNIQUE (ID_usuario, ID_perfil)
);

DROP TABLE IF EXISTS Acesso.pf_se CASCADE;
CREATE TABLE Acesso.pf_se
(
  ID_pf_se   SERIAL,
  ID_perfil  INT NOT NULL,
  ID_servico INT NOT NULL,
  CONSTRAINT PK_pf_se PRIMARY KEY (ID_pf_se),
  CONSTRAINT FK_pf_se_perfil
    FOREIGN KEY (ID_perfil) REFERENCES Acesso.Perfil(ID_perfil)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT FK_pf_se_servico 
    FOREIGN KEY (ID_servico) REFERENCES Acesso.Servico(ID_servico)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_pf_se_perfil_servico UNIQUE (ID_perfil, ID_servico)
);
