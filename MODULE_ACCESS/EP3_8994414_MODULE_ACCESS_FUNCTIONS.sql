\c explorausp_ma


/*
 * CREATE
 */

CREATE OR REPLACE FUNCTION insere_usuario (email email, senha TEXT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Acesso.Usuario (Email, Senha) VALUES ($1, crypt($2, gen_salt('bf')))
      RETURNING ID_usuario INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_perfil (Nome TEXT, Descricao TEXT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Acesso.Perfil (Nome, Descricao) VALUES ($1, $2)
      RETURNING ID_perfil INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_servico (Nome TEXT, Descricao TEXT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Acesso.Servico (Nome, Descricao) VALUES ($1, $2)
      RETURNING ID_servico INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_us_pf (us_email email, pf_nome TEXT)
RETURNS INT AS
$$
  DECLARE
    id_us INT;
    id_pf  INT;
    id INT;
  BEGIN
    SELECT ID_usuario INTO id_us FROM Acesso.Usuario WHERE Email = $1;

    SELECT ID_perfil INTO id_pf FROM Acesso.Perfil WHERE Nome = $2;

    INSERT INTO Acesso.us_pf (ID_usuario, ID_perfil) VALUES (id_us, id_pf)
      RETURNING ID_us_pf INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_pf_se (Nome_pf TEXT, Nome_se TEXT)
RETURNS INT AS
$$
  DECLARE
    id_perf INT;
    id_serv INT;
    id INT;
  BEGIN
    SELECT ID_perfil INTO id_perf FROM Acesso.Perfil WHERE Nome = $1;

    SELECT ID_servico INTO id_serv FROM Acesso.Servico WHERE Nome = $2;

    INSERT INTO Acesso.pf_se (ID_perfil, ID_servico) VALUES (id_perf, id_serv)
      RETURNING ID_pf_se INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * UPDATE
 */

CREATE OR REPLACE FUNCTION atualiza_usuario (
  ant_email email,
  ant_senha TEXT,
  nov_email email DEFAULT NULL,
  nov_senha TEXT  DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT * INTO old FROM Acesso.Usuario WHERE Email = $1 AND
      Senha = public.crypt($2, Senha);

    WITH updated AS (
      UPDATE Acesso.Usuario AS u
        SET
          Email = COALESCE($3, old.Email),
          Senha = COALESCE(crypt($4, gen_salt('bf')), old.Senha)
        WHERE u.ID_usuario = old.ID_usuario RETURNING u.ID_usuario
    ) SELECT INTO id ID_usuario FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION atualiza_perfil (
  nome        TEXT,
  n_nome      TEXT DEFAULT NULL,
  n_descricao TEXT DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT INTO old * FROM Acesso.Perfil AS u WHERE u.Nome = $1;

    WITH updated AS (
      UPDATE Acesso.Perfil AS u
        SET
          Nome      = COALESCE($2, old.Nome),
          Descricao = COALESCE($3, old.Descricao)
        WHERE u.ID_perfil = old.ID_perfil RETURNING u.ID_perfil
    ) SELECT INTO id ID_perfil FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_servico (
  nome        TEXT,
  n_nome      TEXT DEFAULT NULL,
  n_descricao TEXT DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT INTO old * FROM Acesso.Servico AS u WHERE u.Nome = $1;

    WITH updated AS (
      UPDATE Acesso.Servico AS u
        SET
          Nome      = COALESCE($2, old.Nome),
          Descricao = COALESCE($3, old.Descricao)
        WHERE u.ID_servico = old.ID_servico RETURNING u.ID_servico
    ) SELECT INTO id ID_servico FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * DELETE
 */

CREATE OR REPLACE FUNCTION remove_usuario (u_email email, u_senha TEXT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
  WITH deleted AS (
    DELETE FROM Acesso.Usuario WHERE Email = $1 AND Senha = public.crypt($2, Senha) RETURNING *
  ) SELECT INTO n COUNT(*) FROM deleted;

  RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_perfil (Nome TEXT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Acesso.Perfil AS u WHERE u.Nome = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_servico (Nome TEXT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Acesso.Servico AS u WHERE u.Nome = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_us_pf (us_email email, pf_nome TEXT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_us INT;
    id_pf  INT;
    n INT;
  BEGIN
    SELECT ID_usuario INTO id_us FROM Acesso.Usuario WHERE Email = $1;

    SELECT ID_perfil INTO id_pf FROM Acesso.Perfil WHERE Nome = $2;

    WITH deleted AS (
      DELETE FROM Acesso.us_pf WHERE ID_usuario = id_us AND ID_perfil = id_pf RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_pf_se (pf_nome TEXT, se_nome TEXT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_pf  INT;
    id_se INT;
    n INT;
  BEGIN
    SELECT ID_perfil INTO id_pf FROM Acesso.Perfil WHERE Nome = $1;

    SELECT ID_servico INTO id_se FROM Acesso.Servico WHERE Nome = $2;

    WITH deleted AS (
      DELETE FROM Acesso.pf_se WHERE ID_perfil = id_pf AND ID_servico = id_se RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * RETRIEVAL
 */

CREATE OR REPLACE FUNCTION checa_credenciais (us_email email, us_senha TEXT)
RETURNS INTEGER AS
$$
  DECLARE
    id INTEGER;
  BEGIN
    SELECT ID_usuario INTO id FROM Acesso.Usuario
      WHERE Email = $1 AND Senha = public.crypt($2, Senha);

    RETURN id;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION usuario_existe (id INT)
RETURNS BOOLEAN AS
$$
  BEGIN
    RETURN COUNT(*) FROM Acesso.Usuario WHERE ID_usuario = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION perfil_usuario (id INTEGER)
RETURNS VARCHAR AS
$$
  DECLARE
    perfil VARCHAR;
  BEGIN
    SELECT pf.Nome INTO perfil FROM Acesso.Perfil AS pf 
      JOIN Acesso.us_pf AS up ON pf.ID_perfil = up.ID_perfil AND up.ID_usuario = $1; 
    
    RETURN perfil;
  END;
$$
LANGUAGE plpgsql;
