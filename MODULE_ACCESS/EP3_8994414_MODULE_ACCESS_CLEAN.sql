\c explorausp_ma

DELETE FROM Acesso.Usuario;
ALTER SEQUENCE Acesso.Usuario_id_usuario_seq RESTART WITH 1;

DELETE FROM Acesso.Perfil; 
ALTER SEQUENCE Acesso.Perfil_id_perfil_seq RESTART WITH 1;

DELETE FROM Acesso.Servico; 
ALTER SEQUENCE Acesso.Servico_id_servico_seq RESTART WITH 1;

DELETE FROM Acesso.us_pf; 
ALTER SEQUENCE Acesso.us_pf_id_us_pf_seq RESTART WITH 1;

DELETE FROM Acesso.pf_se; 
ALTER SEQUENCE Acesso.pf_se_id_pf_se_seq RESTART WITH 1;
