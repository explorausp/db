INTEGRANTES DO GRUPO

Stephanie Eun Ji Bang   NUSP 8994414

_________________________________________________


INSTRUÇÕES PARA INSTALAÇÃO E EXECUÇÃO

1. Pré-requisitos

  A execução do sistema necessita que as portas 3000 e 8080 da sua máquina estejam disponíveis e
  que o PosgreSQL esteja rodando na porta padrão dele, a 5432.
  Também é necessário que o Docker esteja instalado na sua máquina.

  Os links para instalar o Docker de acordo com a sua SO são listados abaixo:

    - Ubuntu: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
    - Debian: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
    - Fedora: https://docs.docker.com/install/linux/docker-ce/fedora/
    - ArchLinux: https://wiki.archlinux.org/index.php/Docker

  Outros tipos de instalação podem ser encontrados no link https://docs.docker.com/install/.

  A conexão com o PostgreSQL é feita com o usuário 'dba', de senha 'dba1234', que foi criado com
  o código fornecido para o EP2 no arquivo SUPPORT.sql e é apresentado a seguir: 

    CREATE EXTENSION IF NOT EXISTS pgcrypto;
    CREATE EXTENSION IF NOT EXISTS citext;
    -- For security create admin schema as well
    CREATE ROLE dba
      WITH SUPERUSER CREATEDB CREATEROLE
      LOGIN ENCRYPTED PASSWORD 'dba1234'
      VALID UNTIL '2020-07-01';
    CREATE SCHEMA IF NOT EXISTS admins;
    GRANT admins TO dba;
    /*
    If you get something like:
    ERROR:  permission denied to create extension "pgcrypto"
    HINT:  Must be superuser to create this extension.
    It means that the current user does not permissions to create extensions,
    in that case, log out and use the postgres superuser our grant superuser
    permission to yourself.
    Usually you can log as postgres by doing:
    $sudo su - postgres
    $psql -d <NAME_OF_THE_DATABASE>
    Example:
    decio@laptop:~$ sudo su - postgres
    postgres@laptop$ psql -d test
    psql (9.5.16)
    Type "help" for help.

    test=# CREATE EXTENSION IF NOT EXISTS pgcrypto;
    CREATE EXTENSION
    test=# \q
    */

2. API

  Para executar a API, é necessário apenas entrar no diretório ./API/api e rodar os comandos

    docker build . -t ExploraUsp-api
    docker run -it -p 3000:3000 --network host ExploraUsp-api
  
  Caso os comandos retornem o erro "Got permission denied while trying to connect to the Docker
  daemon socket [...]", rodar os comandos com sudo:
  
    sudo docker build . -t ExploraUsp-api
    sudo docker run -it -p 3000:3000 --network host ExploraUsp-api
  
  A api ficará acessível na porta 3000 da sua máquina.

3. Front

  Para executar o front-end, é necessário apenas entrar no diretório ./API/front e rodar os comandos

    docker build . -t ExploraUsp-front
    docker run -it -p 8080:8080 ExploraUsp-front
  
  Caso os comandos retornem o erro "Got permission denied while trying to connect to the Docker
  daemon socket [...]", rodar os comandos com sudo:
  
    sudo docker build . -t ExploraUsp-front
    sudo docker run -it -p 8080:8080 ExploraUsp-front
  
  O front-end ficará acessível na porta 8080 da sua máquina.

_________________________________________________


INFORMAÇÕES ADICIONAIS SOBRE O FUNCIONAMENTO DO SISTEMA

Com a execução dos arquivos DML, alguns dos usuários que estão disponíveis para fazer login no
sistema são apresentados a seguir:

  + ------------- + ----------------------- + ------------- +
  | PERFIL        | EMAIL                   | SENHA         |
  + ------------- + ----------------------- + ------------- +
  | Aluno         | aluno@teste.com         | aluno         |
  | Professor     | professor@teste.com     | professor     |
  | Coordenador   | coordenador@teste.com   | coordenador   |
  | Administrador | administrador@teste.com | administrador |
  + ------------- + ----------------------- + ------------- +

_________________________________________________


INSTRUÇÕES PARA DESINSTALAÇÃO

Para remover as imagens da API e do front, basta rodar os comandos (se necessário, com sudo):

  docker rmi ExploraUsp-api
  docker rmi ExploraUsp-front

Caso os comandos retornem a mensagem de erro "Error response from daemon: conflict: unable to
remove repository reference "nome_da_image" (must force) - container [...]', pode-se forçar a
remoção com a flag -f:

  docker rmi -f ExploraUsp-api
  docker rmi -f ExploraUsp-front
