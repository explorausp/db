\c explorausp_mp

DELETE FROM Pessoas.Pessoa;
ALTER SEQUENCE Pessoas.Pessoa_id_pessoa_seq RESTART WITH 1;

DELETE FROM Pessoas.Aluno; 
ALTER SEQUENCE Pessoas.Aluno_id_Aluno_seq RESTART WITH 1;

DELETE FROM Pessoas.Professor; 
ALTER SEQUENCE Pessoas.Professor_id_professor_seq RESTART WITH 1;

DELETE FROM Pessoas.Administrador; 
ALTER SEQUENCE Pessoas.Administrador_id_administrador_seq RESTART WITH 1;
