CREATE DATABASE ExploraUsp_MP;
\c explorausp_mp
CREATE SCHEMA IF NOT EXISTS Pessoas;


-- Checagem de email
CREATE EXTENSION IF NOT EXISTS pgcrypto;
CREATE EXTENSION IF NOT EXISTS citext;
DROP DOMAIN IF EXISTS email CASCADE;
CREATE DOMAIN email AS citext
  CHECK ( value ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$' );


DROP TABLE IF EXISTS Pessoas.Pessoa CASCADE;
CREATE TABLE Pessoas.Pessoa
(
  ID_pessoa            SERIAL,
  NUSP                 INT          NOT NULL,
  Nome                 VARCHAR(100) NOT NULL,
  CPF                  BIGINT       NOT NULL,
  Data_nascimento      DATE         NOT NULL,
  Endereco_logradouro  VARCHAR(100) NOT NULL,
  Endereco_numero      INT          NOT NULL,
  Endereco_complemento VARCHAR(100),
  Endereco_CEP         INT          NOT NULL,
  Instituto            VARCHAR(100) NOT NULL,
  Email                email        NOT NULL,
  Data_admissao        DATE         NOT NULL,
  CONSTRAINT PK_pessoa      PRIMARY KEY (ID_pessoa),
  CONSTRAINT SK_pessoa_NUSP UNIQUE (NUSP),
  CONSTRAINT SK_pessoa_CPF  UNIQUE (CPF),
  CONSTRAINT checa_NUSP       CHECK (NUSP > 0 AND NUSP < 100000000),
  CONSTRAINT checa_CPF        CHECK (CPF > 9999999999 AND NUSP < 100000000000),
  CONSTRAINT checa_end_numero CHECK (Endereco_numero > 0 AND Endereco_numero < 1000000),
  CONSTRAINT checa_end_CEP    CHECK (Endereco_CEP > 9999999 AND Endereco_CEP < 100000000)
);

DROP TABLE IF EXISTS Pessoas.Aluno CASCADE;
CREATE TABLE Pessoas.Aluno
(
  ID_aluno  SERIAL,
  ID_pessoa INT          NOT NULL,
  Curso     VARCHAR(100) NOT NULL,
  CONSTRAINT PK_aluno        PRIMARY KEY (ID_aluno),
  CONSTRAINT SK_aluno_pessoa UNIQUE (ID_pessoa),
  CONSTRAINT FK_aluno_pessoa
    FOREIGN KEY (ID_pessoa) REFERENCES Pessoas.Pessoa(ID_pessoa)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_aluno_alun_pess UNIQUE (ID_aluno, ID_pessoa)
);

DROP TABLE IF EXISTS Pessoas.Professor CASCADE;
CREATE TABLE Pessoas.Professor
(
  ID_professor SERIAL,
  ID_pessoa    INT          NOT NULL,
  Sala         VARCHAR(10),
  Departamento VARCHAR(100) NOT NULL,
  Telefone     BIGINT,
  CONSTRAINT PK_professor        PRIMARY KEY (ID_professor),
  CONSTRAINT SK_professor_pessoa UNIQUE (ID_pessoa),
  CONSTRAINT FK_professor_pessoa
    FOREIGN KEY (ID_pessoa) REFERENCES Pessoas.Pessoa(ID_pessoa)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_professor_prof_pess UNIQUE (ID_professor, ID_pessoa),
  CONSTRAINT checa_telefone CHECK (Telefone > 999999999 AND Telefone < 100000000000)
);

DROP TABLE IF EXISTS Pessoas.Administrador CASCADE;
CREATE TABLE Pessoas.Administrador
(
  ID_administrador SERIAL,
  ID_pessoa        INT          NOT NULL,
  Departamento     VARCHAR(100) NOT NULL,
  CONSTRAINT PK_administrador        PRIMARY KEY (ID_administrador),
  CONSTRAINT SK_administrador_pessoa UNIQUE (ID_pessoa),
  CONSTRAINT FK_administrador_pessoa
    FOREIGN KEY (ID_pessoa) REFERENCES Pessoas.Pessoa(ID_pessoa)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_administrador_admin_pess UNIQUE (ID_administrador, ID_pessoa)
);
