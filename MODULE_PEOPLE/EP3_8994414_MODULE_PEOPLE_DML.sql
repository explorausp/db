\c explorausp_mp

INSERT INTO Pessoas.Pessoa 
  (NUSP, Nome, Instituto, Email, Data_admissao, CPF, Data_nascimento, Endereco_logradouro, 
   Endereco_numero, Endereco_complemento, Endereco_CEP)
  VALUES 
  (3346200, 'Albert Einstein', 'Instituto de Matemática e Estatística', 'relatividade@cern.com',
    '1997-02-19', 11111111111, '1940-04-12', 'Rua Aaa', 5123, NULL, 11000100),
  (1725933, 'Ho Chi Minh', 'Faculdade de Arquitetura e Urbanismo', 'viet@war.vn',
    '2003-09-08', 11111111112, '1951-07-26', 'Rua Bbb', 987, 'Apto 35', 11000102),
  (8499411, 'Gandalf', 'Escola de Comunicação e Artes', 'oldman@sauron.com',
    '2012-01-06', 11111111113, '1956-05-24', 'Av. Kdsudhas', 123, 'Apt. 201', 10030001),
  (3354662, 'Luke Skywalker', 'Instituto de Física de São Carlos', 'iamyourfather@deathstar.com.br',
    '2007-04-11', 11111111114, '1971-09-16', 'R. Isdjhaksh Ushdjahsgd', 6572, NULL, 10102010),
  (4522676, 'Captain Kirk', 'Insituto de Geociências', 'woosh@enterprise.de',
    '2012-05-04', 11111111115, '1971-10-06', 'Avenida Uasdkja Usjd Upoipoc', 654, NULL, 10103100),
  (5226388, 'Hodor', 'Instituto de Biociências', 'holdthedoor@got.com.ch',
    '2010-03-09', 11111111116, '1975-11-26', 'Asdf dsfjhsdf sd,jfhds', 98, 'welrj', 10003010),
  (8244165, 'Draco Malfoy', 'Instituto de Matemática e Estatística', 'iamnotbad@hogwarts.uk',
    '2000-01-31', 11111111117, '1979-12-31', 'Jhdksajdh asjdh ajshdg', 1000, NULL, 20001110),
  (9631752, 'Anita Garibaldi', 'Escola Politécnica', 'liberati@revolution.fr',
    '1997-05-18', 11111111118, '1982-05-07', 'skjdhasdjhasd askjd asdj', 198572, NULL, 20001001),
  (1793108, 'Anton Sokolov', 'Escola de Comunicação e Artes','shocky@tesla.hr',
    '1998-12-23', 11111111119, '1984-07-31', 'Av. Jsdifu Adsa B. Afds', 10561, 'Apto. 111', 12301020),
  (1557815, 'Eren Jaeger', 'Faculdade de Filosofia, Letras e Ciências Humanas','titanhere@wall.titan',
    '2010-07-10', 11111111120, '1989-04-19', 'Rua Aaa', 5123, NULL, 11000100),
  (1, 'Aluno Teste', 'Instituto de Matemática e Estatística', 'aluno@teste.com',
    '2018-01-01', 99999999990, '1980-01-01', 'Rua Teste A', 123, 'Apto 1', 11111110),
  (2, 'Professor Teste', 'Instituto de Matemática e Estatística', 'professor@teste.com',
    '2010-02-02', 99999999991, '1950-01-01', 'Rua Teste B', 123, 'Apto 1', 11111111),
  (3, 'Coordenador Teste', 'Instituto de Matemática e Estatística', 'coordenador@teste.com',
    '2010-03-03', 99999999992, '1960-01-01', 'Rua Teste C', 123, 'Apto 1', 11111112),
  (4, 'Administrador Teste', 'Instituto de Matemática e Estatística', 'administrador@teste.com',
    '2011-04-04', 99999999993, '1970-01-01', 'Rua Teste D', 123, 'Apto 1', 11111113);

INSERT INTO Pessoas.Aluno
  (ID_pessoa, Curso)
  VALUES 
  (1,  'Ciência da Computação'),
  (2,  'Arquitetura'),
  (3,  'Estatítisca'),
  (4,  'Física'),
  (11, 'Ciência da Computação');

INSERT INTO Pessoas.Professor
  (ID_pessoa, Sala, Departamento, Telefone)
  VALUES 
  (5,  'S-22',  'Geologia Marciana',     11994516123),
  (6,  'E121',  'Jornalismo',            NULL),
  (7,  'C-01',  'Ciência da Computação', 1235643245),
  (12, 'C-100', 'Ciência da Computação', 1132222221),
  (13, 'C-111', 'Ciência da Computação', NULL);

INSERT INTO Pessoas.Administrador
  (ID_pessoa, Departamento)
  VALUES 
  (8,  'Engenharia Mecânica'),
  (9,  'Jornalismo'),
  (10, 'Geografia'),
  (14, 'Ciência da Computação');
