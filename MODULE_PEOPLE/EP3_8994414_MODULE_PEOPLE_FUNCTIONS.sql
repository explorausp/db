\c explorausp_mp


/*
 * CREATE
 */

-- Função auxiliar que insere uma pessoa (não é chamada diretamente pelos perfis)
CREATE OR REPLACE FUNCTION insere_pessoa (
  nusp            INT,
  nome            VARCHAR,
  instituto       VARCHAR,
  email           email,
  cpf             BIGINT,
  data_nascimento DATE,
  data_admissao   DATE,
  end_logr        VARCHAR,
  end_num         INT,
  end_compl       VARCHAR,
  end_cep         INT
) RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Pessoas.Pessoa (NUSP, Nome, Instituto, Email, CPF, Data_nascimento, Data_admissao,
      Endereco_logradouro, Endereco_numero, Endereco_complemento, Endereco_CEP)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
      RETURNING ID_pessoa INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_aluno (
  nusp            INT,
  nome            VARCHAR,
  instituto       VARCHAR,
  email           email,
  cpf             BIGINT,
  data_nascimento DATE,
  data_admissao   DATE,
  end_logr        VARCHAR,
  end_num         INT,
  end_compl       VARCHAR,
  end_cep         INT,
  curso           VARCHAR
) RETURNS TABLE (ID_pessoa INT, ID_aluno INT) AS
$$
  DECLARE
    id INT;
  BEGIN
    id = insere_pessoa ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);

    INSERT INTO Pessoas.Aluno AS a (ID_pessoa, Curso) VALUES (id, $12)
      RETURNING a.ID_aluno INTO id;

    RETURN QUERY SELECT
      a.ID_pessoa,
      a.ID_aluno
    FROM Pessoas.Aluno AS a WHERE a.ID_aluno = id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_professor (
  nusp            INT,
  nome            VARCHAR,
  instituto       VARCHAR,
  email           email,
  cpf             BIGINT,
  data_nascimento DATE,
  data_admissao   DATE,
  end_logr        VARCHAR,
  end_num         INT,
  end_compl       VARCHAR,
  end_cep         INT,
  sala            VARCHAR,
  departamento    VARCHAR,
  telefone        BIGINT
) RETURNS TABLE (ID_pessoa INT, ID_professor INT) AS
$$
  DECLARE
    id INT;
  BEGIN
    id = insere_pessoa ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);

    INSERT INTO Pessoas.Professor AS p (ID_pessoa, Sala, Departamento, Telefone) VALUES
      (id, $12, $13, $14) RETURNING p.ID_professor INTO id;

    RETURN QUERY SELECT
      p.ID_pessoa,
      p.ID_professor
    FROM Pessoas.Professor AS p WHERE p.ID_professor = id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_administrador (
  nusp            INT,
  nome            VARCHAR,
  instituto       VARCHAR,
  email           email,
  cpf             BIGINT,
  data_nascimento DATE,
  data_admissao   DATE,
  end_logr        VARCHAR,
  end_num         INT,
  end_compl       VARCHAR,
  end_cep         INT,
  departamento    VARCHAR
) RETURNS TABLE (ID_pessoa INT, ID_administrador INT) AS
$$
  DECLARE
    id INT;
  BEGIN
    id = insere_pessoa ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);

    INSERT INTO Pessoas.Administrador AS a (ID_pessoa, Departamento) VALUES (id, $12)
      RETURNING a.ID_administrador INTO id;

    RETURN QUERY SELECT
      a.ID_pessoa,
      a.ID_administrador
    FROM Pessoas.Administrador AS a WHERE a.ID_administrador = id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * UPDATE
 */

-- Função auxiliar que atualiza uma pessoa (não é chamada diretamente pelos perfis)
CREATE OR REPLACE FUNCTION atualiza_pessoa (
  nusp              INT,
  n_nusp            INT     DEFAULT NULL,
  n_nome            VARCHAR DEFAULT NULL,
  n_instituto       VARCHAR DEFAULT NULL,
  n_cpf             BIGINT  DEFAULT NULL,
  n_data_nascimento DATE    DEFAULT NULL,
  n_email           VARCHAR DEFAULT NULL,
  n_data_admissao   DATE    DEFAULT NULL,
  n_end_logr        VARCHAR DEFAULT NULL,
  n_end_num         INT     DEFAULT NULL,
  n_end_compl       VARCHAR DEFAULT NULL,
  n_end_cep         INT     DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT INTO old * FROM Pessoas.Pessoa AS p WHERE p.NUSP = $1;

    WITH updated AS (
      UPDATE Pessoas.Pessoa AS p
        SET
          NUSP                 = COALESCE($2,  old.NUSP),
          Nome                 = COALESCE($3,  old.Nome),
          Instituto            = COALESCE($4,  old.Instituto),
          CPF                  = COALESCE($5,  old.CPF),
          Data_nascimento      = COALESCE($6,  old.Data_nascimento),
          Email                = COALESCE($7,  old.Email),
          Data_admissao        = COALESCE($8,  old.Data_admissao),
          Endereco_logradouro  = COALESCE($9,  old.Endereco_logradouro),
          Endereco_numero      = COALESCE($10, old.Endereco_numero),
          Endereco_complemento = COALESCE($11, old.Endereco_complemento),
          Endereco_CEP         = COALESCE($12, old.Endereco_CEP)
        WHERE p.ID_pessoa = old.ID_pessoa RETURNING p.ID_pessoa
    ) SELECT INTO id ID_pessoa FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_aluno (
  nusp              INT,
  n_nusp            INT     DEFAULT NULL,
  n_nome            VARCHAR DEFAULT NULL,
  n_instituto       VARCHAR DEFAULT NULL,
  n_cpf             BIGINT  DEFAULT NULL,
  n_data_nascimento DATE    DEFAULT NULL,
  n_email           VARCHAR DEFAULT NULL,
  n_data_admissao   DATE    DEFAULT NULL,
  n_end_logr        VARCHAR DEFAULT NULL,
  n_end_num         INT     DEFAULT NULL,
  n_end_compl       VARCHAR DEFAULT NULL,
  n_end_cep         INT     DEFAULT NULL,
  n_curso           VARCHAR DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    id INT;
    old RECORD;
  BEGIN
    id = atualiza_pessoa ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);

    SELECT INTO old * FROM Pessoas.Aluno WHERE ID_pessoa = id;
    
    WITH updated AS (
      UPDATE Pessoas.Aluno as a
      SET
        Curso = COALESCE($13, old.Curso)
      WHERE a.ID_aluno = old.ID_aluno RETURNING a.ID_aluno
    ) SELECT INTO id ID_aluno FROM updated;

    RETURN id;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_professor (
  nusp              INT,
  n_nusp            INT     DEFAULT NULL,
  n_nome            VARCHAR DEFAULT NULL,
  n_instituto       VARCHAR DEFAULT NULL,
  n_cpf             BIGINT  DEFAULT NULL,
  n_data_nascimento DATE    DEFAULT NULL,
  n_email           VARCHAR DEFAULT NULL,
  n_data_admissao   DATE    DEFAULT NULL,
  n_end_logr        VARCHAR DEFAULT NULL,
  n_end_num         INT     DEFAULT NULL,
  n_end_compl       VARCHAR DEFAULT NULL,
  n_end_cep         INT     DEFAULT NULL,
  n_sala            VARCHAR DEFAULT NULL,
  n_departamento    VARCHAR DEFAULT NULL,
  n_telefone        INT     DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    id INT;
    old RECORD;
  BEGIN
    id = atualiza_pessoa ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);

    SELECT INTO old * FROM Pessoas.Professor WHERE ID_pessoa = id;
    
    WITH updated AS (
      UPDATE Pessoas.Professor as p
      SET
        Sala         = COALESCE($13, old.Sala),
        Departamento = COALESCE($14, old.Departamento),
        Telefone     = COALESCE($15, old.Telefone)
      WHERE p.ID_professor = old.ID_professor RETURNING p.ID_professor
    ) SELECT INTO id ID_professor FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_administrador (
  nusp              INT,
  n_nusp            INT     DEFAULT NULL,
  n_nome            VARCHAR DEFAULT NULL,
  n_instituto       VARCHAR DEFAULT NULL,
  n_cpf             BIGINT  DEFAULT NULL,
  n_data_nascimento DATE    DEFAULT NULL,
  n_email           VARCHAR DEFAULT NULL,
  n_data_admissao   DATE    DEFAULT NULL,
  n_end_logr        VARCHAR DEFAULT NULL,
  n_end_num         INT     DEFAULT NULL,
  n_end_compl       VARCHAR DEFAULT NULL,
  n_end_cep         INT     DEFAULT NULL,
  n_departamento    VARCHAR DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    id INT;
    old RECORD;
  BEGIN
    id = atualiza_pessoa ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);

    SELECT INTO old * FROM Pessoas.Administrador WHERE ID_pessoa = id;
    
    WITH updated AS (
      UPDATE Pessoas.Administrador as a
      SET
        Departamento = COALESCE($13, old.Departamento)
      WHERE a.ID_administrador = old.ID_administrador RETURNING a.ID_administrador
    ) SELECT INTO id ID_administrador FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * DELETE
 */

CREATE OR REPLACE FUNCTION remove_pessoa (nusp INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Pessoas.Pessoa AS p WHERE p.NUSP = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_aluno (nusp INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_al INT;
    n INT;
  BEGIN
    SELECT a.ID_aluno INTO id_al FROM Pessoas.Pessoa AS p
      JOIN Pessoas.Aluno AS a
      ON p.NUSP = $1 AND p.ID_pessoa = a.ID_pessoa;
    
    WITH deleted AS (
      DELETE FROM Pessoas.Aluno WHERE ID_aluno = id_al RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_professor (nusp INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_prof INT;
    n       INT;
  BEGIN
    SELECT pf.ID_professor INTO id_prof FROM Pessoas.Pessoa AS p
      JOIN Pessoas.Professor AS pf
      ON p.NUSP = $1 AND p.ID_pessoa = pf.ID_pessoa;
    
    WITH deleted AS (
      DELETE FROM Pessoas.Professor WHERE ID_professor = id_prof RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_administrador (nusp INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_adm INT;
    n INT;
  BEGIN
    SELECT a.ID_administrador INTO id_adm FROM Pessoas.Pessoa AS p
      JOIN Pessoas.Administrador AS a
      ON p.NUSP = $1 AND p.ID_pessoa = a.ID_pessoa;
    
    WITH deleted AS (
      DELETE FROM Pessoas.Administrador WHERE ID_administrador = id_adm RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * RETRIEVAL
 */

-- Função auxiliar que retorna info de uma pessoa (não é chamada diretamente pelos perfis)
CREATE OR REPLACE FUNCTION info_pessoa (id_usuario INT) RETURNS TABLE (
  NUSP                 INT,
  Nome                 VARCHAR,
  Instituto            VARCHAR,
  Email                email,
  CPF                  BIGINT,
  Data_nascimento      DATE,
  Data_admissao        DATE,
  Endereco_logradouro  VARCHAR,
  Endereco_numero      INT,
  Endereco_complemento VARCHAR,
  Endereco_CEP         INT
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      p.NUSP,
      p.Nome,
      p.Instituto,
      p.Email,
      p.CPF,
      p.Data_nascimento,
      p.Data_admissao,
      p.Endereco_logradouro,
      p.Endereco_numero,
      p.Endereco_complemento,
      p.Endereco_CEP
    FROM Pessoas.Pessoa AS p WHERE p.ID_pessoa = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION info_aluno (id_pessoa INT) RETURNS TABLE (
  NUSP            INT,
  Nome            VARCHAR,
  Instituto       VARCHAR,
  Email           email,
  CPF             BIGINT,
  Data_nascimento DATE,
  Data_admissao   DATE,
  End_logradouro  VARCHAR,
  End_numero      INT,
  End_complemento VARCHAR,
  End_CEP         INT,
  Curso           VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      p.NUSP,
      p.Nome,
      p.Instituto,
      p.Email,
      p.CPF,
      p.Data_nascimento,
      p.Data_admissao,
      p.Endereco_logradouro,
      p.Endereco_numero,
      p.Endereco_complemento,
      p.Endereco_CEP,
      a.Curso
    FROM (SELECT * FROM info_pessoa ($1)) AS p
    JOIN Pessoas.Aluno AS a ON a.ID_pessoa = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION info_professor (id_pessoa INT) RETURNS TABLE (
  NUSP            INT,
  Nome            VARCHAR,
  Instituto       VARCHAR,
  Email           email,
  CPF             BIGINT,
  Data_nascimento DATE,
  Data_admissao   DATE,
  End_logradouro  VARCHAR,
  End_numero      INT,
  End_complemento VARCHAR,
  End_CEP         INT,
  Sala            VARCHAR,
  Departamento    VARCHAR,
  Telefone        BIGINT
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      p.NUSP,
      p.Nome,
      p.Instituto,
      p.Email,
      p.CPF,
      p.Data_nascimento,
      p.Data_admissao,
      p.Endereco_logradouro,
      p.Endereco_numero,
      p.Endereco_complemento,
      p.Endereco_CEP,
      f.Sala,
      f.Departamento,
      f.Telefone
    FROM (SELECT * FROM info_pessoa ($1)) AS p
    JOIN Pessoas.Professor AS f ON f.ID_pessoa = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION info_administrador (id_pessoa INTEGER) RETURNS TABLE (
  NUSP            INT,
  Nome            VARCHAR,
  Instituto       VARCHAR,
  Email           email,
  CPF             BIGINT,
  Data_nascimento DATE,
  Data_admissao   DATE,
  End_logradouro  VARCHAR,
  End_numero      INT,
  End_complemento VARCHAR,
  End_CEP         INT,
  Departamento    VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      p.NUSP,
      p.Nome,
      p.Instituto,
      p.Email,
      p.CPF,
      p.Data_nascimento,
      p.Data_admissao,
      p.Endereco_logradouro,
      p.Endereco_numero,
      p.Endereco_complemento,
      p.Endereco_CEP,
      a.Departamento
    FROM (SELECT * FROM info_pessoa ($1)) AS p
      JOIN Pessoas.Administrador AS a ON a.ID_pessoa = $1;
  END;
$$
LANGUAGE plpgsql;
