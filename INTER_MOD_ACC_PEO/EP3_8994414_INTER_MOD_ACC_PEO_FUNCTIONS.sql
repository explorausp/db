\c explorausp_map


/*
 * CREATE
 */

CREATE OR REPLACE FUNCTION insere_pe_us (id_pe INT, id_us INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Integracao_AP.pe_us (ID_pessoa, ID_usuario) VALUES (id_pe, id_us)
      RETURNING ID_pe_us INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * DELETE
 */

CREATE OR REPLACE FUNCTION remove_pe_us (id_pe INT, id_us INT)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Integracao_AP.pe_us WHERE ID_pessoa = id_pe AND ID_usuario = id_us RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;
