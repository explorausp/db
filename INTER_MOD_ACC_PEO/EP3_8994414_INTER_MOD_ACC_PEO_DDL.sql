CREATE DATABASE ExploraUSP_MAP;
\c explorausp_map
CREATE SCHEMA IF NOT EXISTS Integracao_AP;


DROP TABLE IF EXISTS Integracao_AP.pe_us CASCADE;
CREATE TABLE Integracao_AP.pe_us
(
  ID_pe_us   SERIAL,
  ID_pessoa  INT NOT NULL,
  ID_usuario INT NOT NULL,
  CONSTRAINT PK_pe_us PRIMARY KEY (ID_pe_us),
  CONSTRAINT CK_pe_us_pessoa_usuario UNIQUE (ID_pessoa, ID_usuario)
);
