\c explorausp_mc


/*
 * CREATE
 */

CREATE OR REPLACE FUNCTION insere_disciplina (sigla VARCHAR, nome VARCHAR, c_aula INT,
  c_trabalho INT, ementa VARCHAR, departamento VARCHAR, instituto VARCHAR)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Curriculos.Disciplina
      (Sigla, Nome, Creditos_aula, Creditos_trabalho, Ementa, Departamento, Instituto)
      VALUES ($1, $2, $3, $4, $5, $6, $7)
      RETURNING ID_disciplina INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_modulo (nome VARCHAR, materias INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Curriculos.Modulo (Nome, Qntd_minima_materias) VALUES ($1, $2)
      RETURNING ID_modulo INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_trilha (nome VARCHAR, modulos INT, optativas INT)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Curriculos.Trilha (Nome, Qntd_minima_modulos, Qntd_minima_optativas)
      VALUES ($1, $2, $3)
      RETURNING ID_trilha INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_curriculo (nome VARCHAR, curso VARCHAR)
RETURNS INT AS
$$
  DECLARE
    id INT;
  BEGIN
    INSERT INTO Curriculos.Curriculo (Nome, Curso)
      VALUES ($1, $2)
      RETURNING ID_curriculo INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_requisito (sigla_principal VARCHAR, sigla_requisito VARCHAR)
RETURNS INT AS
$$
  DECLARE
    id_princ INT;
    id_req INT;
    id INT;
  BEGIN
    SELECT ID_disciplina INTO id_princ FROM Curriculos.Disciplina WHERE Sigla = $1;
    
    SELECT ID_disciplina INTO id_req FROM Curriculos.Disciplina WHERE Sigla = $2;

    INSERT INTO Curriculos.Requisito (ID_disc_principal, ID_disc_requisito) VALUES (id_princ, id_req)
      RETURNING ID_requisito INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_dis_mod (sigla VARCHAR, nome_modulo VARCHAR, obrigatoria BOOLEAN)
RETURNS INT AS
$$
  DECLARE
    id_dis INT;
    id_mod INT;
    id INT;
  BEGIN
    SELECT d.ID_disciplina INTO id_dis FROM Curriculos.Disciplina AS d WHERE d.Sigla = $1;

    SELECT ID_modulo INTO id_mod FROM Curriculos.Modulo WHERE Nome = $2;

    INSERT INTO Curriculos.dis_mod (ID_disciplina, ID_modulo, Obrigatoria)
      VALUES (id_dis, id_mod, $3)
      RETURNING ID_dis_mod INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_tr_mo (nome_modulo VARCHAR, nome_trilha VARCHAR)
RETURNS INT AS
$$
  DECLARE
    id_mo INT;
    id_tr INT;
    id INT;
  BEGIN
    SELECT ID_modulo INTO id_mo FROM Curriculos.Modulo WHERE Nome = $1;

    SELECT ID_trilha INTO id_tr FROM Curriculos.Trilha WHERE Nome = $2;

    INSERT INTO Curriculos.tr_mo (ID_modulo, ID_trilha) VALUES (id_mo, id_tr)
      RETURNING ID_tr_mo INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION insere_rel_cur_tri (nome_curriculo VARCHAR, nome_trilha VARCHAR)
RETURNS INT AS
$$
  DECLARE
    id_cur INT;
    id_tri INT;
    id INT;
  BEGIN
    SELECT ID_curriculo INTO id_cur FROM Curriculos.Curriculo WHERE Nome = $1;

    SELECT ID_trilha INTO id_tri FROM Curriculos.Trilha WHERE Nome = $2;

    INSERT INTO Curriculos.rel_cur_tri (ID_curriculo, ID_trilha) VALUES (id_cur, id_tri)
      RETURNING ID_rel_cur_tri INTO id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * UPDATE
 */

CREATE OR REPLACE FUNCTION atualiza_disciplina (
  sigla           VARCHAR,
  n_sigla         VARCHAR DEFAULT NULL,
  n_nome          VARCHAR DEFAULT NULL,
  n_cred_aula     INT     DEFAULT NULL,
  n_cred_trabalho INT     DEFAULT NULL,
  n_ementa        VARCHAR DEFAULT NULL,
  n_departamento  VARCHAR DEFAULT NULL,
  n_instituto     VARCHAR DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT INTO old * FROM Curriculos.Disciplina AS d WHERE d.Sigla = $1;

    WITH updated AS (
      UPDATE Curriculos.Disciplina AS d
        SET
          Sigla             = COALESCE($2, old.Sigla),
          Nome              = COALESCE($3, old.Nome),
          Creditos_aula     = COALESCE($4, old.Creditos_aula),
          Creditos_trabalho = COALESCE($5, old.Creditos_trabalho),
          Ementa            = COALESCE($6, old.Ementa),
          Departamento      = COALESCE($7, old.Departamento),
          Instituto         = COALESCE($8, old.Instituto)
        WHERE d.ID_disciplina = old.ID_disciplina RETURNING d.ID_disciplina
    ) SELECT INTO id ID_disciplina FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_modulo (
  nome       VARCHAR,
  n_nome     VARCHAR DEFAULT NULL,
  n_qntd_min INT     DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT INTO old * FROM Curriculos.Modulo AS m WHERE m.Nome = $1;

    WITH updated AS (
      UPDATE Curriculos.Modulo AS m
      SET
        Nome                 = COALESCE($2, old.Nome),
        Qntd_minima_materias = COALESCE($3, old.Qntd_minima_materias)
      WHERE m.ID_modulo = old.ID_modulo RETURNING m.ID_modulo
    ) SELECT INTO id ID_modulo FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_trilha (
  nome             VARCHAR,
  n_nome           VARCHAR DEFAULT NULL,
  n_qntd_modulos   INT     DEFAULT NULL,
  n_qntd_optativas INT     DEFAULT NULL
) RETURNS INT AS
$$
  DECLARE
    old RECORD;
    id INT;
  BEGIN
    SELECT INTO old * FROM Curriculos.Trilha AS t WHERE t.Nome = $1;

    WITH updated AS (
      UPDATE Curriculos.Trilha as t
      SET
        Nome                  = COALESCE($2, old.Nome),
        Qntd_minima_modulos   = COALESCE($3, old.Qntd_minima_modulos),
        Qntd_minima_optativas = COALESCE($4, old.Qntd_minima_optativas)
      WHERE t.ID_trilha = old.ID_trilha RETURNING t.ID_trilha
    ) SELECT INTO id ID_trilha FROM updated;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION atualiza_dis_mod (
  sigla         VARCHAR,
  nome_modulo   VARCHAR,
  n_obrigatoria BOOLEAN
) RETURNS INT AS
$$
  DECLARE
    id_dis INT;
    id_mod INT;
    id INT;
  BEGIN
    SELECT d.ID_disciplina INTO id_dis FROM Curriculos.Disciplina AS d WHERE d.Sigla = $1;

    SELECT ID_modulo INTO id_mod FROM Curriculos.Modulo WHERE Nome = $2;

    SELECT ID_dis_mod INTO id FROM Curriculos.dis_mod WHERE ID_disciplina = id_dis AND
      ID_modulo = id_mod;

    UPDATE Curriculos.dis_mod
    SET
      Obrigatoria = $3
    WHERE ID_dis_mod = id;

    RETURN id;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * DELETE
 */

CREATE OR REPLACE FUNCTION remove_disciplina (sigla VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Curriculos.Disciplina AS d WHERE d.Sigla = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_modulo (nome VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Curriculos.Modulo AS m WHERE m.Nome = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_trilha (nome VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Curriculos.Trilha AS t WHERE t.Nome = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_curriculo (nome VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    n INT;
  BEGIN
    WITH deleted AS (
      DELETE FROM Curriculos.Curriculo AS c WHERE c.Nome = $1 RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_requisito (sigla_principal VARCHAR, sigla_requisito VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_pri INT;
    id_req INT;
    n INT;
  BEGIN
    SELECT ID_disciplina INTO id_pri FROM Curriculos.Disciplina WHERE Sigla = $1;
    
    SELECT ID_disciplina INTO id_req FROM Curriculos.Disciplina WHERE Sigla = $2;

    WITH deleted AS (
      DELETE FROM Curriculos.Requisito WHERE ID_disc_principal = id_pri AND
      ID_disc_requisito = id_req RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_dis_mod (sigla VARCHAR, nome_modulo VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_dis INT;
    id_mod INT;
    n INT;
  BEGIN
    SELECT d.ID_disciplina INTO id_dis FROM Curriculos.Disciplina AS d WHERE d.Sigla = $1;

    SELECT ID_modulo INTO id_mod FROM Curriculos.Modulo WHERE Nome = $2;

    WITH deleted AS (
      DELETE FROM Curriculos.dis_mod WHERE ID_disciplina = id_dis AND ID_modulo = id_mod
      RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_tr_mo (nome_modulo VARCHAR, nome_trilha VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_mo INT;
    id_tr INT;
    n INT;
  BEGIN
    SELECT ID_modulo INTO id_mo FROM Curriculos.Modulo WHERE Nome = $1;

    SELECT ID_trilha INTO id_tr FROM Curriculos.Trilha WHERE Nome = $2;

    WITH deleted AS (
      DELETE FROM Curriculos.tr_mo WHERE ID_modulo = id_mo AND ID_trilha = id_tr RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION remove_rel_cur_tri (nome_curriculo VARCHAR, nome_trilha VARCHAR)
RETURNS BOOLEAN AS
$$
  DECLARE
    id_cur INT;
    id_tri INT;
    n INT;
  BEGIN
    SELECT ID_curriculo INTO id_cur FROM Curriculos.Curriculo WHERE Nome = $1;

    SELECT ID_trilha INTO id_tri FROM Curriculos.Trilha WHERE Nome = $2;

    WITH deleted AS (
      DELETE FROM Curriculos.rel_cur_tri WHERE ID_curriculo = id_cur AND ID_trilha = id_tri
        RETURNING *
    ) SELECT INTO n COUNT(*) FROM deleted;

    RETURN n > 0;
  END;
$$ 
LANGUAGE plpgsql;


/*
 * RETRIEVAL
 */

CREATE OR REPLACE FUNCTION curriculos_do_curso (nome_curso VARCHAR) RETURNS TABLE (
  ID   INT,
  Nome VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      c.ID_Curriculo,
      c.Nome
    FROM Curriculos.Curriculo AS c WHERE LOWER(c.Curso) = LOWER($1);
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION trilhas_do_curriculo (id_curr INT) RETURNS TABLE (
  ID                    INT,
  Nome                  VARCHAR,
  Qntd_minima_modulos   INT,
  Qntd_minima_optativas INT
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      t.ID_Trilha,
      t.Nome,
      t.Qntd_minima_modulos,
      t.Qntd_minima_optativas
    FROM Curriculos.Trilha AS t
      JOIN Curriculos.rel_cur_tri AS rct ON rct.ID_Trilha = t.ID_Trilha
      JOIN Curriculos.Curriculo AS c ON c.ID_Curriculo = rct.ID_Curriculo AND c.ID_Curriculo = $1;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION modulos_da_trilha (id_tri INT) RETURNS TABLE (
  ID                   INT,
  Nome                 VARCHAR,
  Qntd_minima_materias INT
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      m.ID_modulo,
      m.Nome,
      m.Qntd_minima_materias
    FROM Curriculos.Modulo AS m
      JOIN Curriculos.tr_mo AS tm ON tm.ID_modulo = m.ID_modulo
      JOIN Curriculos.Trilha AS t ON t.ID_trilha = tm.ID_trilha AND t.ID_Trilha = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION disciplinas_do_modulo (id_mod INT) RETURNS TABLE (
  ID    INT,
  Nome  VARCHAR,
  Sigla VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      d.ID_disciplina,
      d.Nome,
      d.Sigla
    FROM Curriculos.Disciplina AS d
      JOIN Curriculos.dis_mod AS dm ON dm.ID_disciplina = d.ID_disciplina AND dm.ID_modulo = $1;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION disciplinas () RETURNS TABLE (
  ID                INT,
  Sigla             VARCHAR,
  Nome              VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      d.ID_disciplina,
      d.Sigla,
      d.Nome
    FROM Curriculos.Disciplina AS d;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION disciplina (id_dis INT) RETURNS TABLE (
  Sigla             VARCHAR,
  Nome              VARCHAR,
  Creditos_aula     INT,
  Creditos_trabalho INT,
  Ementa            VARCHAR,
  Departamento      VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      d.Sigla,
      d.Nome,
      d.Creditos_aula,
      d.Creditos_trabalho,
      d.Ementa,
      d.Departamento
    FROM Curriculos.Disciplina AS d WHERE d.ID_disciplina = $1;
  END;
$$ 
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION disciplinas_do_instituto (nome_instituto VARCHAR) RETURNS TABLE (
  Sigla             VARCHAR,
  Nome              VARCHAR,
  Creditos_aula     INT,
  Creditos_trabalho INT,
  Ementa            VARCHAR,
  Departamento      VARCHAR
) AS
$$
  BEGIN
    RETURN QUERY SELECT
      d.Sigla,
      d.Nome,
      d.Creditos_aula,
      d.Creditos_trabalho,
      d.Ementa,
      d.Departamento
    FROM Curriculos.Disciplina AS d WHERE LOWER(d.Instituto) = LOWER($1);
  END;
$$
LANGUAGE plpgsql;
