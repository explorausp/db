CREATE DATABASE ExploraUsp_MC;
\c explorausp_mc
CREATE SCHEMA IF NOT EXISTS Curriculos;


DROP TABLE IF EXISTS Curriculos.Disciplina CASCADE;
CREATE TABLE Curriculos.Disciplina
(
  ID_disciplina     SERIAL,
  Sigla             VARCHAR(7)    NOT NULL,
  Nome              VARCHAR(100)  NOT NULL,
  Creditos_aula     INT           DEFAULT 0,
  Creditos_trabalho INT           DEFAULT 0,
  Ementa            VARCHAR(1000) NOT NULL,
  Departamento      VARCHAR(100)  NOT NULL,
  Instituto         VARCHAR(100)  NOT NULL,
  CONSTRAINT PK_disciplina           PRIMARY KEY (ID_disciplina),
  CONSTRAINT SK_disciplina_sigla     UNIQUE (Sigla),
  CONSTRAINT checa_creditos_aula     CHECK (Creditos_aula >= 0),
  CONSTRAINT checa_creditos_trabalho CHECK (Creditos_trabalho >= 0)
);

DROP TABLE IF EXISTS Curriculos.Modulo CASCADE;
CREATE TABLE Curriculos.Modulo
(
  ID_modulo            SERIAL,
  Nome                 VARCHAR(100) NOT NULL,
  Qntd_minima_materias INT          DEFAULT NULL,
  CONSTRAINT PK_modulo                  PRIMARY KEY (ID_modulo),
  CONSTRAINT SK_modulo_nome             UNIQUE (Nome),
  CONSTRAINT checa_qntd_minima_materias CHECK (Qntd_minima_materias >= 0)
);

DROP TABLE IF EXISTS Curriculos.Trilha CASCADE;
CREATE TABLE Curriculos.Trilha
(
  ID_trilha             SERIAL,
  Nome                  VARCHAR(100) NOT NULL,
  Qntd_minima_modulos   INT          DEFAULT 0,
  Qntd_minima_optativas INT          DEFAULT NULL,
  CONSTRAINT PK_trilha                   PRIMARY KEY (ID_trilha),
  CONSTRAINT SK_trilha_nome              UNIQUE (Nome),
  CONSTRAINT checa_qntd_minima_modulos   CHECK (Qntd_minima_modulos >= 0),
  CONSTRAINT checa_qntd_minima_optativas CHECK (Qntd_minima_optativas >= 0)
);

DROP TABLE IF EXISTS Curriculos.Curriculo CASCADE;
CREATE TABLE Curriculos.Curriculo
(
  ID_curriculo SERIAL,
  Nome         VARCHAR(100) NOT NULL,
  Curso        VARCHAR(100) NOT NULL,
  CONSTRAINT PK_curriculo      PRIMARY KEY (ID_curriculo),
  CONSTRAINT SK_curriculo_nome UNIQUE (Nome)
);

DROP TABLE IF EXISTS Curriculos.Requisito CASCADE;
CREATE TABLE Curriculos.Requisito
(
  ID_requisito      SERIAL,
  ID_disc_principal INT    NOT NULL,
  ID_disc_requisito INT    NOT NULL,
  CONSTRAINT PK_requisito PRIMARY KEY (ID_requisito),
  CONSTRAINT FK_requisito_disc_principal
    FOREIGN KEY (ID_disc_principal) REFERENCES Curriculos.Disciplina(ID_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT FK_requisito_disc_requisito 
    FOREIGN KEY (ID_disc_requisito) REFERENCES Curriculos.Disciplina(ID_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_requisito_princ_requis UNIQUE (ID_disc_principal, ID_disc_requisito),
  CONSTRAINT checa_disciplinas_diferentes CHECK (ID_disc_principal <> ID_disc_requisito)
);

DROP TABLE IF EXISTS Curriculos.dis_mod CASCADE;
CREATE TABLE Curriculos.dis_mod
(
  ID_dis_mod     SERIAL,
  ID_disciplina  INT     NOT NULL,
  ID_modulo      INT     NOT NULL,
  Obrigatoria    BOOLEAN DEFAULT FALSE,
  CONSTRAINT PK_dis_mod PRIMARY KEY (ID_dis_mod),
  CONSTRAINT FK_dis_mod_disciplina
    FOREIGN KEY (ID_disciplina) REFERENCES Curriculos.Disciplina(ID_disciplina)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT FK_dis_mod_modulo 
    FOREIGN KEY (ID_modulo)     REFERENCES Curriculos.Modulo(ID_modulo)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_dis_mod_discipl_modulo UNIQUE (ID_disciplina, ID_modulo)
);

DROP TABLE IF EXISTS Curriculos.tr_mo CASCADE;
CREATE TABLE Curriculos.tr_mo
(
  ID_tr_mo  SERIAL,
  ID_trilha INT    NOT NULL,
  ID_modulo INT    NOT NULL,
  CONSTRAINT PK_tr_mo PRIMARY KEY (ID_tr_mo),
  CONSTRAINT FK_tr_mo_trilha
    FOREIGN KEY (ID_trilha) REFERENCES Curriculos.Trilha(ID_trilha)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT FK_tr_mo_modulo 
    FOREIGN KEY (ID_modulo) REFERENCES Curriculos.Modulo(ID_modulo)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_tr_mo_trilha_modulo UNIQUE (ID_trilha, ID_modulo)
);

DROP TABLE IF EXISTS Curriculos.rel_cur_tri CASCADE;
CREATE TABLE Curriculos.rel_cur_tri
(
  ID_rel_cur_tri SERIAL,
  ID_curriculo   INT NOT NULL,
  ID_trilha      INT NOT NULL,
  CONSTRAINT PK_rel_cur_tri PRIMARY KEY (ID_rel_cur_tri),
  CONSTRAINT FK_rel_cur_tri_curriculo
    FOREIGN KEY (ID_curriculo) REFERENCES Curriculos.Curriculo(ID_curriculo)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT FK_rel_cur_tri_trilha 
    FOREIGN KEY (ID_trilha) REFERENCES Curriculos.Trilha(ID_trilha)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  CONSTRAINT CK_rel_cur_tri_curr_tri UNIQUE (ID_curriculo, ID_trilha)
);
