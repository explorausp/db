\c explorausp_mc

DELETE FROM Curriculos.Disciplina; 
ALTER SEQUENCE Curriculos.Disciplina_id_disciplina_seq RESTART WITH 1;

DELETE FROM Curriculos.Modulo; 
ALTER SEQUENCE Curriculos.Modulo_id_modulo_seq RESTART WITH 1;

DELETE FROM Curriculos.Trilha; 
ALTER SEQUENCE Curriculos.Trilha_id_trilha_seq RESTART WITH 1;

DELETE FROM Curriculos.Curriculo; 
ALTER SEQUENCE Curriculos.Curriculo_id_curriculo_seq RESTART WITH 1;

DELETE FROM Curriculos.Requisito; 
ALTER SEQUENCE Curriculos.Requisito_id_requisito_seq RESTART WITH 1;

DELETE FROM Curriculos.dis_mod; 
ALTER SEQUENCE Curriculos.dis_mod_id_dis_mod_seq RESTART WITH 1;

DELETE FROM Curriculos.tr_mo; 
ALTER SEQUENCE Curriculos.tr_mo_id_tr_mo_seq RESTART WITH 1;

DELETE FROM Curriculos.rel_cur_tri; 
ALTER SEQUENCE Curriculos.rel_cur_tri_id_rel_cur_tri_seq RESTART WITH 1;
