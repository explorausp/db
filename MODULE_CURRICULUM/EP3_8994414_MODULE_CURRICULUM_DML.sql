\c explorausp_mc

INSERT INTO Curriculos.Disciplina
  (Sigla, Nome, Creditos_aula, Creditos_trabalho, Ementa, Departamento, Instituto)
  VALUES
  ('LCF0224', 'Dendrologia', 4, 0, '.Introdução e importância. Localização das plantas produtoras de madeira no Reino Vegetal. Dendrologia das angiospermas(folhosas) nativas e exóticas: ocorrência no país de origem e distribuição no Brasil; características da organografia e anatomia, crescimento e desenvolvimento em diferentes condições, variação existentes entre e dentro das espécies, reconhecimento através dos constituintes do vegetal.Dentrologia das gymnospermas (coníferas) nativas e exóticas:ocorrência no país de origem e distribuição no Brasil; características da organografia e anatomia, crescimento e desenvolvimento em diferentes condições, variação existente entre e dentro das espécies, reconhecimento através dos constituintes do vegetal.', 'Ciências Florestais', 'Escola Superior de Agricultura "Luiz de Queiroz"'),
  ('EDA0689', 'Estágio de Vivência e Investigação em Gestão Escolar e Políticas Públicas', 1, 2, 'Estágio curricular de vivência e investigação numa unidade escolar como escola-campo, ou em outro espaço educativo, sob supervisão, que auxilie no percurso formativo do graduando, dando-lhe uma visão mais conjunta e crítica das discussões teóricas e práticas no que concerne ao impacto das políticas públicas na gestão de unidade escolar.', 'Adm Escolar e Economia da Educação', 'Faculdade de Educação'),
  ('CMU0403', 'Canto II', 1, 0, 'Iniciação ao Canto de maneira individual. Desenvolvimento de uma técnica vocal em atendimento individualizado que permita ao aluno o domínio do Canto.', 'Música', 'Escola de Comunicações e Artes'),
  ('DPM0517', 'Criminologia Clínica e Execução Penal', 2, 0, 'Bases históricas do pensamento clínico-criminológico. Conceituação de criminologia clínica. Modelos de criminologia clínica e suas aplicações à execução penal. As avaliações técnicas na execução penal. A reintegração social dos encarcerados.', 'Direito Penal, Medicina Forense e Criminologia', 'Faculdade de Direito'),
  ('4703691', 'Ações Comunitárias I', 3, 2, 'Exposição do aluno a situações comunitárias, situações espontâneas ou situações formais (grupos diversos, civis ou governamentais), visando: "Reflexão crítica sobre o processo comunitário.". Conhecimentos teórico-práticos enraizados em experiência comunitária." Propostas de ação comunitária.', 'Disciplinas Interdepartamentais do Instituto de Psicologia', 'Instituto de Psicologia'),
  ('PSE2555', 'Comportamento Animal', 3, 1, 'Panorama dos conceitos e métodos das ciências do comportamento animal: Psicologia Comparativa, Etologia, Ecologia Comportamental.', 'Psicologia Experimental', 'Instituto de Psicologia'),
  ('MAE0413', 'Estatística Aplicada I', 6, 1, '1. Discussão de temas relacionados com assessoria e consultoria estatística. 2. Planejamento e análise de dados provenientes de problemas apresentados ao Centro de Estatística Aplicada do IME/USP.', 'Estatística', 'Instituto de Matemática e Estatística'),
  ('MAC0328', 'Algoritmos em Grafos', 4, 0, 'Conexão de grafos e digrafos. Emparelhamentos máximos. Fluxo máximo. Coloração de vértices. Circuitos hamiltonianos. Tópicos opcionais.', 'Ciência da Computação', 'Instituto de Matemática e Estatística'),
  ('FLC0143', 'Conceitos de Poética Latina', 2, 1, 'O curso está dividido em duas partes: a primeira em que se discutem os conceitos de poética entre os gregos e a segunda em que se discutem os latinos. Primeiro, examina-se a crítica platônica à poesia na República. Em seguida, estuda-se a Poética de Aristóteles, em que se discutem os critérios pelos quais se separam os gêneros – meios, modos e objetos –, a poesia como imitação e teoria do mito. Na segunda metade, vêem-se os principais conceitos na Arte Poética de Horácio, como unidade, decoro, elocução e métrica. Na parte final do curso, estudam-se os gêneros de elocução, “as fronteiras” entre os gêneros de poesia e os conceitos alexandrinos de imitação e emulação.', 'Letras Clássicas e Vernáculas', 'Faculdade de Filosofia, Letras e Ciências Humanas'),
  ('MAC0343', 'Otimização Semidefinida e Aplicações', 4, 0, 'Programação semidefinida é uma poderosa extensão de programação linear. Neste curso vemos a teoria de programação semidefinida bem como inúmeras aplicações em otimização combinatória, teoria de códigos e otimização polinomial. Técnicas de programação semidefinida têm encontrado inúmeras aplicações em matemática, logística, engenharia, etc. Tais técnicas constituem uma parte essencial das ferramentas de otimização disponíveis.', 'Ciência da Computação', 'Instituto de Matemática e Estatística');

INSERT INTO Curriculos.Modulo
  (Nome, Qntd_minima_materias)
  VALUES
  ('Algoritmos',                        2),
  ('Matemática discreta',               3),
  ('Otimização',                        2),
  ('Desenvolvimento de Software',       3),
  ('Banco de Dados',                    2),
  ('Sistemas Paralelos e Distribuídos', 3),
  ('Introdução à IA',                   3),
  ('Inteligência Artificial',           1),
  ('Sistemas',                          2),
  ('Teoria associada à IA',             1),
  ('Matemática Discreta II',            NULL),
  ('Algoritmos II',                     NULL),
  ('Núcleo 1',                          4),
  ('Núcleo 2',                          1);

INSERT INTO Curriculos.Trilha
  (Nome, Qntd_minima_modulos, Qntd_minima_optativas)
  VALUES
  ('Ciência de Dados',        4, NULL),
  ('Inteligência Artificial', 4, NULL),
  ('Sistemas de Software',    3, NULL),
  ('Teoria da Computação',    2, 7   ),
  ('Computação na Nuvem',     3, NULL),
  ('Desenvolvimento Web',     3, NULL),
  ('Aaa',                     2, 6   ),
  ('Bbb',                     2, NULL),
  ('Ccc',                     2, NULL),
  ('Ddd',                     2, NULL);

INSERT INTO Curriculos.Curriculo
  (Nome, Curso)
  VALUES
  ('BCC - 45051', 'Ciência da Computação'),
  ('BCC - 45052', 'Ciência da Computação'),
  ('BCC - 45053', 'Ciência da Computação'),
  ('BCC - 45054', 'Ciência da Computação'),
  ('BCC - 45055', 'Ciência da Computação'),
  ('BCC - 45056', 'Ciência da Computação'),
  ('BCC - 45057', 'Ciência da Computação'),
  ('BCC - 45058', 'Ciência da Computação'),
  ('BCC - 45059', 'Ciência da Computação'),
  ('BCC - 45060', 'Ciência da Computação');

INSERT INTO Curriculos.Requisito
  (ID_disc_principal, ID_disc_requisito)
  VALUES
  (1,  4),
  (2,  4),
  (3,  4),
  (5,  7),
  (6,  7),
  (7,  2),
  (8,  1),
  (8,  9);
  (9,  2),
  (10, 1);

INSERT INTO Curriculos.dis_mod
  (ID_disciplina, ID_modulo, Obrigatoria)
  VALUES
  (1,  1, TRUE),
  (2,  1, TRUE),
  (3,  1, TRUE),
  (4,  1, FALSE),
  (5,  1, FALSE),
  (6,  2, TRUE),
  (7,  2, FALSE),
  (8,  2, TRUE),
  (9,  2, TRUE),
  (10, 2, TRUE);

INSERT INTO Curriculos.tr_mo
  (ID_trilha, ID_modulo)
  VALUES
  (1,  1),
  (2,  2),
  (3,  2),
  (4,  3),
  (5,  3),
  (6,  4),
  (7,  7),
  (8,  7),
  (9,  8),
  (10, 1);

INSERT INTO Curriculos.rel_cur_tri
  (ID_curriculo, ID_trilha)
  VALUES
  (1,  1 ),
  (1,  2 ),
  (1,  3 ),
  (1,  4 ),
  (2,  3 ),
  (2,  6 ),
  (2,  7 ),
  (5,  8 ),
  (5,  9 ),
  (10, 10);
